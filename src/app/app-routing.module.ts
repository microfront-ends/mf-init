import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BoletaComponent } from "./boleta/boleta.component";
import { DocumentComponent } from "./documents/document.component";
import { EmptyRouteComponent } from "./empty-route/empty-route.component";
import { FacturaComponent } from "./factura/factura.component";

const routes: Routes = [
  /*{
    path: "documentos", 
    component: DocumentComponent,
    children: [
      {path: "factura", component: FacturaComponent},
      {path: "boleta", component: BoletaComponent}
    ]
  },*/
  {
    path: "documentos/factura",
    component: FacturaComponent
  },
  {
    path: "documentos/boleta",
    component: BoletaComponent
  },
  { 
    path: "**", 
    component: EmptyRouteComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: "/" }],
})
export class AppRoutingModule {}