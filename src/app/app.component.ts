import { Component } from '@angular/core';
import { PermisoModulo } from 'src/services/init.model';
import { InitService } from 'src/services/init.service';

@Component({
  selector: 'mf-init',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mf-init';

  navType: string;
  themeLayout: string;
  layoutType: string;
  verticalPlacement: string;
  verticalLayout: string;
  deviceType: string;
  verticalNavType: string;
  verticalEffect: string;
  vNavigationView: string;

  headerFixedMargin: string;
  pcodedHeaderPosition: string;
  pcodedSidebarPosition: string;
  navBarTheme: string;
  activeItemTheme: string;

  menuTitleTheme: string;
  itemBorder: boolean;
  itemBorderStyle: string;
  subItemBorder: boolean;
  dropDownIcon: string;
  subItemIcon: string;

  toggleOn: boolean;
  windowWidth: number;

  NOTA_CREDITO = 1005;
  NOTA_DEBITO = 1006;
  COMUNICACION_BAJA = 1011;
  RESUMEN_BOLETAS = 1012;
  DOCUMENTO_COMPRA = 2014;
  GUIA_REMISION = 3005;
  NOTA_CREDITO_PARAMETRO = 5;
  FACTURA = 1003;
  BOLETA = 1004;

  PERMISO_DOCUMENTO_BUSQUEDA = 29;
  PERMISO_DOCUMENTO_COMUNICACION_BAJA = 30;
  PERMISO_DOCUMENTO_FACTURA = 31;
  PERMISO_DOCUMENTO_BOLETA = 134;
  PERMISO_DOCUMENTO_NOTA_CREDITO = 135;
  PERMISO_DOCUMENTO_NOTA_DEBITO = 136;

  PERMISO_CONFIGURCION_ALMACEN = 101;
  PERMISO_CONFIGURCION_ITEM = 42;
  PERMISO_CONFIGURCION_SERIE = 32;
  PERMISO_CONFIGURCION_TERMINALES = 123;

  PERMISO_VENTA_PUNTO_VENTA = 240;
  PERMISO_VENTA_LISTA_PRECIOS = 128;
  PERMISO_VENTA_ESTADO_CAJA = 122;

  PERMISO_LOGISTICA_INGRESO = 39;
  PERMISO_LOGISTICA_SALIDA = 40;
  PERMISO_LOGISTICA_TRASLADO = 41;

  nota_credito = this.NOTA_CREDITO;
  nota_credito_parametro = this.NOTA_CREDITO_PARAMETRO;
  nota_debito = this.NOTA_DEBITO;
  nota_debito_parametro = this.NOTA_CREDITO_PARAMETRO;
  factura = this.FACTURA;
  boleta = this.BOLETA;

  listaPermisos: Array<PermisoModulo> = Array<PermisoModulo>();

  permiso_documento_busqueda = this.PERMISO_DOCUMENTO_BUSQUEDA;
  permiso_documento_comunicacion_baja = this.PERMISO_DOCUMENTO_COMUNICACION_BAJA;
  permiso_documento_factura =  this.PERMISO_DOCUMENTO_FACTURA;
  permiso_documento_boleta = this.PERMISO_DOCUMENTO_BOLETA;
  permiso_documento_nota_credito = this.PERMISO_DOCUMENTO_NOTA_CREDITO;
  permiso_documento_nota_debito = this.PERMISO_DOCUMENTO_NOTA_DEBITO;

  permiso_configuracion_almacen = this.PERMISO_CONFIGURCION_ALMACEN;
  permiso_configuracion_item = this.PERMISO_CONFIGURCION_ITEM;
  permiso_configuracion_serie =  this.PERMISO_CONFIGURCION_SERIE;
  permiso_configuracion_teminales = this.PERMISO_CONFIGURCION_TERMINALES;

  permiso_centa_punto_venta =  this.PERMISO_VENTA_PUNTO_VENTA;
  permiso_centa_lista_precio = this.PERMISO_VENTA_LISTA_PRECIOS;
  permiso_centa_estado_caja = this.PERMISO_VENTA_ESTADO_CAJA;

  permiso_logistica_ingreso =  this.PERMISO_LOGISTICA_INGRESO;
  permiso_logistica_salida = this.PERMISO_LOGISTICA_SALIDA;
  permiso_logistica_traslado = this.PERMISO_LOGISTICA_TRASLADO;

  constructor(private _initService: InitService) {
    this.navType = 'st2';
    this.themeLayout = 'vertical';
    this.verticalPlacement = 'left';
    this.verticalLayout = 'wide';
    this.deviceType = 'desktop';
    this.verticalNavType = 'expanded';
    this.verticalEffect = 'shrink';
    this.vNavigationView = 'view1';

    this.menuTitleTheme = 'theme5';
    this.itemBorder = true;
    this.itemBorderStyle = 'none';
    this.subItemBorder = true;
    this.dropDownIcon = 'style1';
    this.subItemIcon = 'style6';

    this.navBarTheme = 'themelight1';
    this.activeItemTheme = 'theme4';
    this.headerFixedMargin = '0px';
    
    this.pcodedHeaderPosition = 'fixed';
    this.pcodedSidebarPosition = 'fixed';

    this.toggleOn = true;
    this.windowWidth = window.innerWidth;

  }

  ngOnInit(){
    this.litarPermisos();
  }

  litarPermisos() {
    this._initService
    .getSeguridadMenuPrincipal()
    .subscribe(lista => {
      this.listaPermisos = lista;
      console.log("lista de permisos", this.listaPermisos);
    }, error=> {});
  }

  buscarPermiso(algo: any): boolean{
    let respuesta: boolean = false;
    let permisoTemporal: PermisoModulo = new PermisoModulo();
    permisoTemporal = this.listaPermisos.filter(per =>(per.IdPermiso == algo))[0];
    respuesta = permisoTemporal == undefined? false: true;
    return respuesta;
  }

  onResize($event) {

  }
}
