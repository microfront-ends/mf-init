export class PermisoModulo {
    IdPermiso: number;
    IdParent: number;
    Ruta: string;
    Tipo: number;
    IconMenu?: any;
    IdModulo: number;
    SubMenu?: any;
    Nivel: number;
    Hijos: any[];
    Descripcion: string;
    Id: number;
    LogUsuariocrea: string;
    LogFechacrea: Date;
    LogUsuariomodif?: any;
    LogFechamodif: Date;
    LogEstado: number;
}